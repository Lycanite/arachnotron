#!/bin/bash

# Start Directory (Should be project root)
START_DIR=$PWD

# Determine Deploy Directory
if [ -d "realpath $1" ]; then
   DEPLOY_DIR="$1"
else
   DEPLOY_DIR="../deployed"
fi
echo "Deploy dir: $DEPLOY_DIR"

# Determine Release Directory
if [ -d "realpath $2" ]; then
   RELEASE_DIR="$2"
else
   RELEASE_DIR="../builds/release"
fi
echo "Release dir: $RELEASE_DIR"

cd $DEPLOY_DIR

echo "Copying release files..."
cp $RELEASE_DIR/* . -R

echo "Removing build source files..."
rm *.o *.h *.cpp Makefile

# Get Libarary Paths
flag=false
for entry in `ldd $DEPLOY_DIR/*`; do
   if $flag; then
      # Only add to the array if it is not already in it
      if ! [[ $libsArray =~ $entry ]]; then
         echo "adding $entry"
         libsArray="$libsArray $entry"
      fi
      flag=false
   fi

   # Libraries are after "=>"
   if [ $entry == "=>" ]; then
      flag=true
   fi
done
echo 
echo

# Copy Libraries
mkdir -p lib
for entry in $libsArray; do
   echo "cp -v -f $entry $DEPLOY_DIR/lib"
   cp -v -f $entry $DEPLOY_DIR/lib
done

# Copy QT Plugins
QT5_QML_PATH=/usr/lib/x86_64-linux-gnu/qt5/
cp $QT5_QML_PATH/qml/Qt Qt -R
cp $QT5_QML_PATH/qml/QtGraphicalEffects QtGraphicalEffects -R
cp $QT5_QML_PATH/qml/QtQuick QtQuick -R
cp $QT5_QML_PATH/qml/QtQuick.2 QtQuick.2 -R
cp $QT5_QML_PATH/plugins/imageformats imageformats -R
cp $QT5_QML_PATH/plugins/platforms platforms -R

# Copy QML and Config
cp $START_DIR/qml qml -R
cp $START_DIR/config config -R
cp $START_DIR/arachnotron.sh .

# QT Audio Plugin
# mkdir -p lib/audio
# cp -v -f `qmake -query QT_INSTALL_BINS`/../plugins/audio/* $DEPLOY_DIR/lib/audio

cd $START_DIR
