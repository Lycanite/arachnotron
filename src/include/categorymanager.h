#ifndef CATEGORYMANAGER_H
#define CATEGORYMANAGER_H

#include "include/categorymodel.h"

#include <QQmlEngine>

class CategoryManager : public JsonLoader {
    Q_OBJECT

protected:
    // A list of all loaded categories.
    QHash<int, CategoryModel *> categories;

    // A list of valid category IDs.
    QVector<int> categoryIds;

    // The next available category ID to use.
    int nextId = 0;

public:
    // Constructor
    CategoryManager(QQmlEngine * qmlEngine);

    // Initialize
    bool init();

    // Static Reference
    static CategoryManager * instance;

    // QML Engine
    QQmlEngine * qmlEngine;

public slots:
    // JSON
    virtual bool readFromJson() override;
    virtual bool writeToJson() override;
    QString getAbsolutePath(QString);

    // Categories
    QHash<int, CategoryModel *> getCategories();
    QVector<int> getCategoryIds();
    CategoryModel * getCategory(int);
    bool addCategory(CategoryModel *);
    CategoryModel * createCategory();
    CategoryModel * importCategory(QString);
    bool removeCategory(int);
    void clearCategories();
};

#endif
