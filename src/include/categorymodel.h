#ifndef CATEGORYMODEL_H
#define CATEGORYMODEL_H

#include "include/profilemodel.h"

class CategoryModel : public JsonLoader {
    Q_OBJECT

protected:
    // A list of profiles that belong to this category.
    QVector<ProfileModel *> profiles;

public:
    // If true, all profiles are included in this category.
    bool allProfiles = false;

    // Constructor
    CategoryModel();


public slots:
    virtual QString getModelType() override;

    // Setters
    virtual void setString(QString, QString) override;

    // Profiles
    int loadProfiles();
    QVector<ProfileModel *> getProfiles();
    ProfileModel * getProfile(int index);
};

#endif
