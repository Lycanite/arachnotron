#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include "include/jsonloader.h"

#include <QGuiApplication>
#include <QQmlEngine>

class SettingsManager : public JsonLoader {
    Q_OBJECT

protected:
    // The current launcher app name.
    QString appName = "Arachnotron Doom Launcher";

    // The current launcher app version.
    QString appVersion;

    // A list of all engines (indexed by name with a pair of engine path and config path).
    QHash<QString, QPair<QString, QString>> engines;

    // A list of all iwads (indexed by name with a path).
    QHash<QString, QString> iwads;

public:
    // Constructor
    SettingsManager(QQmlEngine *, QGuiApplication *);

    // Initialize
    bool init();

    // JSON
    virtual void parseJson(QString, QJsonValue) override;

    // Static Reference
    static SettingsManager * instance;

public slots:
    // Info
    QString getAppName();
    QString getAppVersion();
    bool isWindows();

    // JSON
    virtual bool writeToJson() override;

    // Import
    QObject * importFile(QString);

    // Engines
    QHash<QString, QPair<QString, QString>> getEngines();
    QList<QString> getEngineKeys();
    void clearEngines();
    QPair<QString, QString> getEngine(QString);
    QString getEnginePath(QString);
    QString getEngineConfig(QString);
    bool addEngine(QString, QString, QString);
    bool removeEngine(QString);

    // Iwads
    bool findIwads(QString);
    QHash<QString, QString> getIwads();
    QList<QString> getIwadKeys();
    void clearIwads();
    QString getIwad(QString);
    bool addIwad(QString, QString);
    bool removeIwad(QString);
};

#endif // SETTINGSMANAGER_H
