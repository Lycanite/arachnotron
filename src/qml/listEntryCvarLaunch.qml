import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Arachnotron 1.0
import "../scripts/listEntry.js" as ListEntry

Item {
    id: cvarEntry
    width: 416
    height: 32

    property var inputFields: [valueInput]
    property var list: null
    property var name: nameLabel
    property var load: ListEntry.loadCvarLaunchEntry;
    property var remove: ListEntry.removeCvarLaunchEntry;

    RowLayout {
        id: cvarLayout
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 10
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 10
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.bottomMargin: 1
        anchors.top: parent ? parent.top : undefined
        anchors.topMargin: 1

        Text {
            id: nameLabel
            height: 27
            color: "#ffffff"
            text: qsTr("CVar Name")
            Layout.fillWidth: true
            Layout.preferredWidth: 204
            font.pointSize: 12
            verticalAlignment: Text.AlignBottom
            lineHeight: 1.22
            font.family: Arachnotron.mainFont
        }

        TextField {
            id: valueInput
            text: qsTr("")
            font.family: Arachnotron.mainFont
            font.pointSize: 12
            selectByMouse: true
            rightPadding: 4
            leftPadding: 4
            bottomPadding: 0
            topPadding: 3
            Layout.fillWidth: true
            Layout.preferredWidth: 150
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
        }

        Button {
            id: resetButton
            text: qsTr("")
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/importIcon.svg"
        }
    }

    Connections {
        target: resetButton
        onClicked: {
            ListEntry.resetCvarLaunchEntry(cvarEntry)
        }
    }
}
