import QtQuick 2.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/profiles.js" as Profiles

Item {
    id: profileButton
    width: 272
    height: 186

    property int profileId: -1;
    property var name: nameLabel;
    property var icon: iconImage;
    property var background: backgroundImage;
    property var backgroundDefault: "../assets/ControlsBG.png";

    MouseArea {
        id: buttonArea
        anchors.fill: parent

        Image {
            id: backgroundImage
            height: 153
            anchors.top: parent ? parent.top : undefined
            anchors.right: parent ? parent.right : undefined
            anchors.left: parent ? parent.left : undefined
            source: backgroundDefault
            fillMode: Image.Tile

            Image {
                id: iconImage
                anchors.fill: parent
                source: "../assets/profile.png"
                fillMode: Image.PreserveAspectFit

                Row {
                    id: buttons
                    spacing: 2
                    layoutDirection: Qt.RightToLeft
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0

                    Button {
                        id: launchButton
                        width: 32
                        height: 32
                        highlighted: true
                        icon.source: "../assets/launchIcon.svg"
                    }

                    Button {
                        id: quickLaunchButton
                        width: 32
                        height: 32
                        highlighted: true
                        icon.source: "../assets/quickLaunchIcon.svg"
                    }

                    Button {
                        id: editButton
                        width: 32
                        height: 32
                        highlighted: true
                        icon.source: "../assets/editIcon.svg"
                    }
                }
            }
        }

        Text {
            id: nameLabel
            height: 24
            color: "#ffffff"
            text: qsTr("The Ultimate Doom")
            font.capitalization: Font.SmallCaps
            font.bold: true
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.right: parent ? parent.right : undefined
            anchors.left: parent ? parent.left : undefined
            anchors.topMargin: 8
            anchors.top: backgroundImage.bottom
        }
    }

    Connections {
        target: buttonArea
        onClicked: Profiles.selectProfile(profileButton)
    }

    Connections {
        target: launchButton
        onClicked: Profiles.selectProfile(profileButton)
    }

    Connections {
        target: quickLaunchButton
        onClicked: Profiles.quickLaunch(profileButton)
    }

    Connections {
        target: editButton
        onClicked: Profiles.quickEdit(profileButton)
    }
}











/*##^## Designer {
    D{i:4;anchors_width:200}
}
 ##^##*/
