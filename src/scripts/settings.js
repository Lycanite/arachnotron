.pragma library
.import "fileLoader.js" as FileLoader
.import "profiles.js" as Profiles
.import "categories.js" as Categories

// This script is for all launcher settings.
var settingsManager;
var settingsView;

function setSettingsManager(manager) {
    settingsManager = manager;
}

function setSettingsView(view) {
    settingsView = view;
}


// Save Settings:
function saveSettings() {
    settingsView.saveData(settingsManager);
    settingsManager.writeToJson();
}


// Import Profile
function importFile() {
    FileLoader.openFileDialog("Import JSON Files", importFromUrl, undefined, undefined, false, false, true);
}

function importFromUrl(target, urls) {
    var lastImportedCategoryButton;
    var lastImportedProfile;

    for(var i = 0; i < urls.length; i++) {
        var importedObj = settingsManager.importFile(urls[i]);
        if(!importedObj) {
            continue;
        }
        if(importedObj.getModelType() === "Category") {
            lastImportedCategoryButton = Categories.onImportCategory(importedObj);
        }
        else if(importedObj.getModelType() === "Profile") {
            lastImportedProfile = importedObj;
        }
    }

    if(lastImportedCategoryButton) {
        Categories.selectCategory(lastImportedCategoryButton);
    }
    else if(lastImportedProfile) {
        Profiles.onImportProfile(lastImportedProfile);
    }
}
