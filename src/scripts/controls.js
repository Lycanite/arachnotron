.pragma library

// This script manages the controls (bottom right set of buttons).

var controlsTarget = null;
function setControlsTarget(target) {
    controlsTarget = target;
}

var controls = null;
function setControls(formQml) {
    // Remove Existing Form:
    if(controls !== null) {
        controls.destroy();
        controls = null;
    }

    // No Form:
    if(formQml === "") {
        return null;
    }

    // Create New Form:
    var component = Qt.createComponent(formQml);
    controls = component.createObject(controlsTarget);
    if(controls === null) {
        console.log("Error creating controls: " + formQml);
        return null;
    }
    controls.anchors.top = controls.parent.top;
    controls.anchors.bottom = controls.parent.bottom;
    controls.anchors.left = controls.parent.left;
    controls.anchors.right = controls.parent.right;

    return controls;
}
