#include "include/categorymanager.h"
#include "include/profilemanager.h"
#include "include/settingsmanager.h"

#include <QGuiApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);


    // Setup QT App:
    QGuiApplication app(argc, argv);
    app.setOrganizationName("Nephrite");
    app.setOrganizationDomain("nephrite.uk");
    app.setApplicationVersion("1.1.4");
    app.setApplicationName("Arachnotron Doom Launcher");
    app.setWindowIcon(QIcon(":/assets/appIcon.png"));

    // Setup QML:
    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:/qml");

    // Setup Data:
    SettingsManager settingsManager(&engine, &app);
    engine.rootContext()->setContextProperty("settingsManager", &settingsManager);
    qmlRegisterType<SettingsManager>();
    qmlRegisterType<ProfileLaunch>();

    CategoryManager categoryManager(&engine);
    engine.rootContext()->setContextProperty("categoryManager", &categoryManager);
    qmlRegisterType<CategoryModel>();

    ProfileManager profileManager(&engine);
    engine.rootContext()->setContextProperty("profileManager", &profileManager);
    qmlRegisterType<ProfileModel>();

    // Init Managers:
    settingsManager.init();
    categoryManager.init();
    profileManager.init();

    // Start QML:
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
